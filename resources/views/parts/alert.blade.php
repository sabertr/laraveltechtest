@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = session()->get('successMessage'))
    <div class="alert alert-success">

            {{ $message }}

    </div>
@endif

@if ($message = session()->get('errorMessage'))
    <div class="alert alert-danger">

            {{ $message }}

    </div>
@endif
