<?php
$selected = \Illuminate\Support\Facades\App::getLocale();
?>
<select name="lang" form="lang-form"   onchange="document.getElementById('lang-form').submit()">
    <option {{$selected ==="fr" ? "selected" : ""}} value="fr">fr</option>
    <option {{$selected ==="en" ? "selected" : ""}} value="en">en</option>
</select>
<form method="POST" action="{{route('lang.set')}}" id="lang-form">
@csrf
</form>

