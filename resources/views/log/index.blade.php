<?php
/** @var  $logs Log[] */

use App\Models\Log;

?>

@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="card-title text-uppercase">User Access Logs</div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Url</th>
                            <th>
                                {{ __('Short Url') }}
                            </th>
                            <th>
                                {{ __('Access date') }}
                            </th>
                            <th>
                                {{ __('Ip Address') }}
                            </th>
                            <th>
                                {{ __('Country') }}
                            </th>
                            <th>
                                {{ __('User Agent') }}
                            </th>
                            <th>
                                {{ __('User') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($logs as $index=>$log)
                            <tr>
                                <th>{{$log->link->url}}</th>
                                <th>{{$log->link->generateShortUrl()}}</th>
                                <th>{{$log->access_date}}</th>
                                <th>{{$log->ip}}</th>
                                <th>{{$log->country}}</th>
                                <th>{{$log->user_agent}}</th>
                                <th>{{$log->user->name?? '(Guest)'}}</th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
@endsection
