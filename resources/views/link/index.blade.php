<?php
/** @var \App\Models\Link[] $links */
?>

@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="card-title text-uppercase">
                        {{__('My Links')}}
                    </div>
                    <a class="btn btn-success" href="{{route('link.create')}}">
                        {{__('Add new link')}}
                    </a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th>{{__('Url')}}</th>
                            <th>{{__('Short Url')}}</th>
                            <th>{{__('Creation date')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($links as $index=>$link)
                            <tr>
                                <th scope="row">{{$index+1}}</th>
                                <th>{{$link->url}}</th>
                                <th>{{$link->generateShortUrl()}}</th>
                                <th>{{$link->created_at}}</th>
                                <th class="actions">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button form="{{$formId=uniqid('', false)}}" type="submit"  class="btn btn-danger" data-toggle="tooltip"
                                                data-placement="top" title="{{__('Delete')}}">
                                            <i class="far fa-minus-square"></i>
                                        </button>
                                        <form id="{{$formId}}" method="POST"  action="{{route('link.destroy',['link'=>$link->id])}}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
@endsection
