@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <form id="link-form" method="POST" action="{{route('link.store')}}" class="form-row">
                @csrf
                <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">{{ __('Url') }}</label>
                    <input type="text" class="form-control" name="url" placeholder="{{ __('Enter Url') }}">
                </div>
                <button type="submit" class="btn btn-primary">{{ __('Create') }}</button>
            </form>
        </div>
    </div>
@endsection
