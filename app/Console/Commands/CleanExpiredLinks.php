<?php

namespace App\Console\Commands;

use App\Models\Link;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanExpiredLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'link:clean-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete links older than 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $countDeleted = Link::query()->expired()->delete();
        $countDeleted
            ?    $this->info("successful $countDeleted expired links.")
            :    $this->info("Nothing to delete.");
    }
}
