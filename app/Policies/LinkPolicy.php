<?php

namespace App\Policies;

use App\Models\Link;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class LinkPolicy
{
    use HandlesAuthorization;

    private const MAX_LINK_PER_USER = 5;


    public function delete(User $user,? Link $link=null)
    {
        if($link !== null){
            return $user->id === $link->user_id;
        }
        return true;
    }


    public function create(User $user)
    {
        $maxReached =  $user->links()->count() >= self::MAX_LINK_PER_USER;
        if($maxReached){
            return Response::deny('You can create a maximum of 5 links.');
        }
        return Response::allow();
    }




}
