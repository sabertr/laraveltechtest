<?php

namespace App\Observers;

use App\Models\Link;
use Illuminate\Support\Facades\Auth;

class LinkObserver
{

    private const MAX_APPLICATION_LINK = 20;

    public function saving(Link $link)
    {
        $link->user()->associate(Auth::user());
        $this->ensureApplicationMaxLinks();
    }

    public function ensureApplicationMaxLinks(): void
    {
        $countAppLinks = Link::query()->count();
        $isMaxReached = $countAppLinks >= self::MAX_APPLICATION_LINK;
        if ($isMaxReached) {
            $offset = $countAppLinks - self::MAX_APPLICATION_LINK + 1;
            Link::query()->oldest()->take($offset)->delete();
        }
    }


}
