<?php

namespace App\Http\Middleware;

use App\Models\Link;
use App\Models\Log;
use Closure;
use Illuminate\Http\Request;

class UserAccessLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $encodedIdParamName)
    {
        $encodedId = $request->route()->parameter($encodedIdParamName);
        $decodedId = $this->decodeId($encodedId);
        $link = Link::query()->findOrFail($decodedId);
        Log::traceRequest($link);
        return $next($request);
    }

    public function decodeId($encodedId)
    {
        $decodedId = base64_decode($encodedId, true);
        $encodedIdRepeat = base64_encode($decodedId);
        if ($encodedIdRepeat !== $encodedId) {
            abort(404);
        }
        return $decodedId;
    }
}
