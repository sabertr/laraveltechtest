<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLinkRequest;
use App\Models\Link;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Gate;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = Link::query()->authUser()->orderBy('created_at','desc')->get();
        return view('link.index', compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('link.create');
    }


    public function store(StoreLinkRequest $request)
    {
        $response = Gate::inspect('create', Link::class);
        if(!$response->allowed()){
            return redirect()->route('link.index')->with('errorMessage',$response->message());
        }

        $link = new Link();
        $link->url = $request->input('url');
        $link->save();
        return redirect()
            ->route('link.index')
            ->with('successMessage', 'saved');
    }


    public function accessEncodedLink($decodedId)
    {
        $link =  Link::query()->find(base64_decode($decodedId));
        return redirect()->to($link->url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Link $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        $this->authorize('delete', $link);
        $link->delete();
        return redirect()
            ->route('link.index')
            ->with('successMessage', 'Deleted!');
    }
}
