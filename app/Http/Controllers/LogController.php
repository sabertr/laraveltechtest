<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index()
    {
        $logs = Log::query()->latest('access_date')->get();
        return view('log.index',compact('logs'));
    }
}
