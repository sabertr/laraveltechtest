<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * @property string ip
 * @property string access_date
 * @property string country
 * @property string user_agent
 * @property integer|null user_id
 */
class Log extends Model
{
    use HasFactory;
    public $timestamps = false;

    public static function traceRequest(Link $link)
    {
        $request = request();
        $log = new Log();
        $ip = $request->ip();
        $isPublicIp = filter_var($ip, FILTER_VALIDATE_IP,FILTER_FLAG_NO_RES_RANGE);

        $log->ip = $ip;
        $log->access_date = Carbon::createFromTimestamp($request->server->get('REQUEST_TIME'))->toDateTimeString();
        $log->country =  $isPublicIp ? file_get_contents("https://ipapi.co/$ip/country_name") : "(reserved)"  ;
        $log->user_agent = $request->server->get('HTTP_USER_AGENT');
        $log->user_id = Auth::id();
        $log->link_id = $link->id;
        $log->save();
    }

    public function link()
    {
        return $this->belongsTo(Link::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
