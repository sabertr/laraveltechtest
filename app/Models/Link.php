<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * @property string url
 */
class Link extends Model
{
    use HasFactory;


    public function scopeAuthUser(Builder $query)
    {
        $query->where('user_id', '=', Auth::id());
    }

    public function scopeExpired(Builder $query)
    {
        $query->where('created_at', '<', Carbon::now()->addHours(-24)->toDateTimeString());
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function generateShortUrl()
    {
        return route('link.access', [
            'encodedId' => base64_encode($this->id)
        ]);
    }
}
