<?php

use App\Http\Controllers\LinkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__ . '/auth.php';




Route::middleware(['auth','web'])->prefix('dashboard')->group(function(){
    Route::get('/', function () {
        return redirect()->route('link.index');
    })->name('dashboard');

    Route::get('/logs', [\App\Http\Controllers\LogController::class, 'index'])
        ->name('log.index')
        ->middleware(['auth']);
    Route::resource('link', \App\Http\Controllers\LinkController::class)->middleware(['auth']);
});

Route::group(['middleware' => ['web']],function (){

    Route::get('/', function () {

        return view('welcome');
    });

    Route::get('/{encodedId}', [LinkController::class, 'accessEncodedLink'])
        ->name('link.access')
        ->middleware(['accessLog:encodedId']);

    Route::post('/lang/set',static function (\Illuminate\Http\Request $request){
        $lang = $request->input('lang');
        $request->session()->put('lang',$lang);
        return redirect()->back();
    })->name('lang.set');

});



Auth::routes();

